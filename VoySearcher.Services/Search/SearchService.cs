﻿using System.Collections.Generic;
using System.Linq;
using VoySearcher.Contracts;
using VoySearcher.Contracts.Interface;
using VoySearcher.Contracts.Models;

namespace VoySearcher.Services.Search
{
	public class SearchService : ISearchService
	{
		private readonly IGoogleSearchService _googleSearchService;
		private readonly IBingSearchService _bingSearchService;

		public SearchService(IGoogleSearchService googleSearchService, IBingSearchService bingSearchService)
		{
			_googleSearchService = googleSearchService;
			_bingSearchService = bingSearchService;
		}

		public IList<SearchResultDTO> GetSearchResultsForAllProviders(string searchQuery)
		{
			var searchArray = TransformSearchQueryToArray(searchQuery);

			long googleCount = searchArray.Sum(sq => _googleSearchService.GetHitCount(sq));
			long bingCount = searchArray.Sum(sq => _bingSearchService.GetHitCount(sq));

			var result = new List<SearchResultDTO>
			{
				new SearchResultDTO {Provider = SearchProviderEnum.Google, SearchResultAmount = googleCount},
				new SearchResultDTO {Provider = SearchProviderEnum.Bing, SearchResultAmount = bingCount}
			};

			return result;
		}

		private static string[] TransformSearchQueryToArray(string searchQuery)
		{
			var result = searchQuery
							.Split(' ')
							.Distinct()
							.Where(w => !string.IsNullOrEmpty(w));

			return result.ToArray();
		}
	}
}
