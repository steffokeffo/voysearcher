﻿using System.Linq;
using Newtonsoft.Json;
using VoySearcher.Contracts;
using VoySearcher.Contracts.Interface;
using VoySearcher.Services.Api;
using VoySearcher.Services.Models;

namespace VoySearcher.Services.Search
{
	public class GoogleSearchService : ApiServiceBase, IGoogleSearchService
	{
		public GoogleSearchService(RestClient restClient) : base(SearchProviderEnum.Google, restClient)
		{
		}

		public long GetHitCount(string searchQuery)
		{
			var result = Client.GetResponse(Provider, searchQuery);

			var jsonObject = JsonConvert.DeserializeObject<GoogleRootobject>(result);

			long.TryParse(jsonObject?.Queries?.Requests?.FirstOrDefault()?.TotalResults, out long hitCount);

			return hitCount;
		}
	}
}
