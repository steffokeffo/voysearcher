﻿using Newtonsoft.Json;
using VoySearcher.Contracts;
using VoySearcher.Contracts.Interface;
using VoySearcher.Services.Api;
using VoySearcher.Services.Models;

namespace VoySearcher.Services.Search
{
	public class BingSearchService : ApiServiceBase, IBingSearchService
	{
		public BingSearchService(RestClient client) : base(SearchProviderEnum.Bing, client)
		{
		}

		public long GetHitCount(string searchQuery)
		{
			var result = Client.GetResponse(Provider, searchQuery);

			var jsonObject = JsonConvert.DeserializeObject<BingRootobject>(result);

			return jsonObject?.WebPages?.HitCount ?? 0;
		}
	}
}
