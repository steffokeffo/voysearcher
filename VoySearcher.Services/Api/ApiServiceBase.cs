﻿using VoySearcher.Contracts;

namespace VoySearcher.Services.Api
{
	public class ApiServiceBase
	{
		protected readonly SearchProviderEnum Provider;
		protected readonly RestClient Client;

		public ApiServiceBase(SearchProviderEnum provider, RestClient client)
		{
			Provider = provider;
			Client = client;
		}
	}
}
