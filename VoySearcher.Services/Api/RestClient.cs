﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using VoySearcher.Contracts;

namespace VoySearcher.Services.Api
{
	public class RestClient
	{
		private readonly string _googleApiKey;
		private readonly string _googleCx;
		private readonly string _bingApiKey;

		public RestClient()
		{
			_googleApiKey = ConfigurationManager.AppSettings["googleApiKey"];
			_googleCx = ConfigurationManager.AppSettings["googleCx"];
			_bingApiKey = ConfigurationManager.AppSettings["bingApiKey"];
		}

		public string GetResponse(SearchProviderEnum provider, string searchQuery)
		{
			var request = WebRequest.Create(GetUrl(provider, searchQuery));

			AddSettingsForProvider(provider, request);
			try
			{
				HttpWebResponse response = (HttpWebResponse)request.GetResponse();
				Stream dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				string responseString = reader.ReadToEnd();

				return responseString;
			}
			catch (WebException ex)
			{
				if (ex.Status != WebExceptionStatus.ProtocolError) 
					throw new Exception(ex.Message);

				if (ex.Response is HttpWebResponse response)
				{
					if (response.StatusCode == (HttpStatusCode)429) //to many requests
					{
						throw new Exception($"Too many requests for provider: {provider} - Please change API key for provider to continue until tomorrow");
					}
				}
				throw new Exception(ex.Message);
			}
		}

		private void AddSettingsForProvider(SearchProviderEnum provider, WebRequest request)
		{
			switch (provider)
			{
				case SearchProviderEnum.Bing:
					request.Headers.Add("Ocp-Apim-Subscription-Key", _bingApiKey);
					break;

				case SearchProviderEnum.Google:
					break;

				default:
					break;
			}
		}

		private string GetUrl(SearchProviderEnum provider, string searchQuery)
		{
			switch (provider)
			{
				case SearchProviderEnum.Google:
					return "https://www.googleapis.com/customsearch/v1?key=" + _googleApiKey + "&cx=" + _googleCx + "&q=" + searchQuery;
				case SearchProviderEnum.Bing:
					return "https://api.bing.microsoft.com/v7.0/search?" + "q=" + searchQuery;
				default:
					return string.Empty;
			}
		}

		//private string GetBingResponse(string searchQuery)
		//{
		//	var client = new WebSearchClient(new ApiKeyServiceClientCredentials(_bingApiKey));

		//	var result = client.Web.SearchAsync(query: searchQuery);

		//	var count = result.Result.WebPages.TotalEstimatedMatches;

		//	return count.HasValue ? count.Value.ToString() : "0" ;
		//}
	}
}
