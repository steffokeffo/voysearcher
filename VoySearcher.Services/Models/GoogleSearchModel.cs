﻿using Newtonsoft.Json;

namespace VoySearcher.Services.Models
{
	public class GoogleRootobject
	{
		[JsonProperty("queries")]
		public Queries Queries { get; set; }
	}

	public class Queries
	{
		[JsonProperty("request")]
		public Request[] Requests { get; set; }
	}

	public class Request
	{
		[JsonProperty("totalResults")]
		public string TotalResults { get; set; }
	}
}
