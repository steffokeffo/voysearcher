﻿using Newtonsoft.Json;

namespace VoySearcher.Services.Models
{
	public class BingRootobject
	{
		[JsonProperty("webPages")]
		public Webpages WebPages { get; set; }
	}

	public class Webpages
	{
		[JsonProperty("totalEstimatedMatches")]
		public long HitCount { get; set; }
	}
}
