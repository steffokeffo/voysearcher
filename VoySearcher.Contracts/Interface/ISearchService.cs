﻿using System.Collections.Generic;
using VoySearcher.Contracts.Models;

namespace VoySearcher.Contracts.Interface
{
	public interface ISearchService
	{
		IList<SearchResultDTO> GetSearchResultsForAllProviders(string searchQuery);
	}
}
