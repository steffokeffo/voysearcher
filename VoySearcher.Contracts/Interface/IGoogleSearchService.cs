﻿namespace VoySearcher.Contracts.Interface
{
	public interface IGoogleSearchService
	{
		long GetHitCount(string searchQuery);
	}
}
