﻿namespace VoySearcher.Contracts.Interface
{
	public interface IBingSearchService
	{
		long GetHitCount(string searchQuery);
	}
}
