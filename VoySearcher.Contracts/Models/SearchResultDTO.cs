﻿namespace VoySearcher.Contracts.Models
{
	public class SearchResultDTO
	{
		public SearchProviderEnum Provider { get; set; }
		public long SearchResultAmount { get; set; }
	}
}
