﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using VoySearcher.Contracts;
using VoySearcher.Contracts.Interface;
using VoySearcher.Contracts.Models;
using VoySearcher.Services.Search;
using Xunit;

namespace VoySearcher.Tests
{
	public class SearchServiceTests
	{
		private readonly ISearchService _sut;
		private readonly IGoogleSearchService _googleSearchService;
		private readonly IBingSearchService _bingSearchService;
		private readonly long _googleHitCount = 500;
		private readonly long _bingHitCount = 30;
		private readonly string _searchQuery = "voyado";

		public SearchServiceTests()
		{
			_googleSearchService = Substitute.For<IGoogleSearchService>();
			_bingSearchService = Substitute.For<IBingSearchService>();

			_googleSearchService.GetHitCount(Arg.Any<string>()).Returns(_googleHitCount);
			_bingSearchService.GetHitCount(Arg.Any<string>()).Returns(_bingHitCount);

			_sut = new SearchService(_googleSearchService, _bingSearchService);
		}

		[Fact]
		public void GetSearchResultsForAllProviders_Should_Not_Return_Null()
		{
			//arrange

			//act
			var result = _sut.GetSearchResultsForAllProviders(_searchQuery);

			//assert
			Assert.NotNull(result);
		}

		[Fact]
		public void GetSearchResultsForAllProviders_Should_Contain_Populated_List()
		{
			//arrange

			//act
			var result = _sut.GetSearchResultsForAllProviders(_searchQuery);

			//assert
			Assert.True(result.Any());
		}

		[Fact]
		public void GetSearchResultsForAllProviders_Should_Return_Two_Providers()
		{
			//arrange
			int expectedAmountOfProviders = 2;

			//act
			var result = _sut.GetSearchResultsForAllProviders(_searchQuery);

			//assert
			Assert.Equal(expectedAmountOfProviders, result.Count);
		}

		[Fact]
		public void GetSearchResultsForAllProviders_Should_Return_Correct_HitAmount()
		{
			//arrange

			//act
			var result = _sut.GetSearchResultsForAllProviders(_searchQuery);

			//assert
			Assert.Equal(_googleHitCount, result.SingleOrDefault(f => f.Provider == SearchProviderEnum.Google).SearchResultAmount);
			Assert.Equal(_bingHitCount, result.SingleOrDefault(f => f.Provider == SearchProviderEnum.Bing).SearchResultAmount);
		}

		[Theory]
		[InlineData("voyado", 1)]
		[InlineData("voyado litium", 2)]
		[InlineData("banan båt äpple", 3)]
		public void GetSearchResultsForAllProviders_Should_Call_Service_Corresponding_Amount_of_Search_Words(string searchQ, int expected)
		{
			//arrange

			//act
			_sut.GetSearchResultsForAllProviders(searchQ);

			//assert
			_googleSearchService.Received(expected).GetHitCount(Arg.Any<string>());
		}
	}
}
