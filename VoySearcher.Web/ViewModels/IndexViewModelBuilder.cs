﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VoySearcher.Contracts.Models;

namespace VoySearcher.Web.ViewModels
{
	public class IndexViewModelBuilder
	{
		public IndexViewModel Build(IList<SearchResultDTO> searchResultDTO, IndexViewModel viewModel)
		{
			return new IndexViewModel
			{
				InputFieldText = viewModel.InputFieldText,
				SearchResultItems = searchResultDTO.Select(sri => new SearchResultItem
				{
					Provider = sri.Provider.ToString(),
					SearchResultAmount = sri.SearchResultAmount.ToString("N0", CultureInfo.CurrentCulture)
				}).ToList()
			};
		}
	}
}