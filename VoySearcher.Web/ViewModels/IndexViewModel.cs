﻿using System.Collections.Generic;

namespace VoySearcher.Web.ViewModels
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			SearchResultItems = new List<SearchResultItem>();
		}

		public string InputFieldText { get; set; }

		public IList<SearchResultItem> SearchResultItems { get; set; }

		public bool HasError{ get; set; }
		public string ErrorMessage { get; set; }
	}

	public class SearchResultItem
	{
		public string Provider { get; set; }
		public string SearchResultAmount { get; set; }
	}
}