﻿using System;
using System.Web.Mvc;
using VoySearcher.Contracts.Interface;
using VoySearcher.Web.ViewModels;

namespace VoySearcher.Web.Controllers
{
	public class HomeController : Controller
	{
		private readonly ISearchService _searchService;
		private readonly IndexViewModelBuilder _indexViewModelBuilder;

		public HomeController(ISearchService searchService, IndexViewModelBuilder indexViewModelBuilder)
		{
			_searchService = searchService;
			_indexViewModelBuilder = indexViewModelBuilder;
		}

		[HttpGet]
		public ActionResult Index()
		{
			var viewModel = new IndexViewModel();

			return View(viewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Index(IndexViewModel model)
		{
			IndexViewModel viewModel;
			try
			{
				var searchDtos = _searchService.GetSearchResultsForAllProviders(model.InputFieldText);

				viewModel = _indexViewModelBuilder.Build(searchDtos, model);

				return View(viewModel);
			}
			catch (Exception ex)
			{
				viewModel = new IndexViewModel
				{
					HasError = true,
					ErrorMessage = ex.Message
				};

				return View(viewModel);
			}
			
		}
	}
}